package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sample.model.Client;
import sample.service.ClientGenerator;
import sample.service.QueueSimulator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Controller {

    @FXML private TextField minArriveTimeBetweenCustomersField;
    @FXML private TextField maxArriveTimeBetweenCustomersField;

    @FXML private TextField minServiceTimePerCustomerField;
    @FXML private TextField maxServiceTimePerCustomerField;

    @FXML private TextField noOfQueuesField;
    @FXML private TextField simulationIntervalField;

    @FXML private Label counterLabel;
    @FXML private Label avgWaitTimeLabel;
    @FXML private Label avgServiceTimeLabel;
    @FXML private Label emptyQueueTimeLabel;

    int minArriveTimeBetweenCustomers, maxArriveTimeBetweenCustomers,  minServiceTimePerCustomer, maxServiceTimePerCustomer, noOfQueues, simulationInterval;

    @FXML private ScrollPane scrollPane;


    @FXML
    private Button startSimulationBtn;



    VBox deskColumns[];
    //desk columns

    //map to retain the deskColumn along with its list of Clients(VBox with info and photo)
    Map<VBox,List<VBox>> deskClientMap;

    public Controller(){
    }

    public void alert(String msg){
        //alert box with a given message
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public void info(String msg){
        //alert box with a given message

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                alert.setContentText(msg);

                alert.showAndWait();
            }
        });
    }


    private boolean checkFields(){
        //check for all fields not to be empty
        if(minArriveTimeBetweenCustomersField.getText().isEmpty() || maxArriveTimeBetweenCustomersField.getText().isEmpty() ||
                minServiceTimePerCustomerField.getText().isEmpty() || maxServiceTimePerCustomerField.getText().isEmpty() ||
                noOfQueuesField.getText().isEmpty() || simulationIntervalField.getText().isEmpty())
            return false;
        return true;
    }


    private boolean checkIntegerValues(){
        //check for all fields to have numerical values
        try {
            minArriveTimeBetweenCustomers = Integer.parseInt(minArriveTimeBetweenCustomersField.getText());
        } catch (NumberFormatException e) {
            alert("Min arrive time between customers should be integer");
            return false;
        }

        try {
            maxArriveTimeBetweenCustomers = Integer.parseInt(maxArriveTimeBetweenCustomersField.getText());
        } catch (NumberFormatException e) {
            alert("Max arrive time between customers should be integer");
            return false;
        }

        if(minArriveTimeBetweenCustomers >= maxArriveTimeBetweenCustomers){
            alert("Min arrive time between customers should me lower than max");
            return false;
        }

        try {
            minServiceTimePerCustomer = Integer.parseInt(minServiceTimePerCustomerField.getText());
        } catch (NumberFormatException e) {
            alert("Min Service Time per Customer should be integer");
            return false;
        }


        try {
            maxServiceTimePerCustomer = Integer.parseInt(maxServiceTimePerCustomerField.getText());
        } catch (NumberFormatException e) {
            alert("Min Service Time per Customer should be integer");
            return false;
        }

        if(minServiceTimePerCustomer >= maxServiceTimePerCustomer){
            alert("Min service time per customers should me lower than max");
            return false;
        }

        try {
            noOfQueues = Integer.parseInt(noOfQueuesField.getText());
        } catch (NumberFormatException e) {
            alert("No of queues should be integer");
            return false;
        }


        try {
            simulationInterval = Integer.parseInt(simulationIntervalField.getText());
        } catch (NumberFormatException e) {
            alert("Simulation interval should be integer");
            return false;
        }


        if(minArriveTimeBetweenCustomers <=0)
        {
            alert("Arrival time between customers should be greater than 0!");
            return false;
        }

        return true;
    }

    @FXML
    private void startSimulationBtnPressed(ActionEvent event) {
        deskClientMap = new HashMap<>();
        if(checkFields()) {
            if(checkIntegerValues()){
                //Values are valid
                HBox boxToFillScrollPane = new HBox(); //box to fill the content of the desks area

                deskColumns = new VBox[noOfQueues]; //an array with the desk columns (VBox)

                for(int i=0;i<noOfQueues;i++){
                    deskColumns[i] = createDeskBox(i+1);  //create Desk Box

                    deskClientMap.put(deskColumns[i], new ArrayList<>()); //add into the map the DeskColumn with an empty list

                    boxToFillScrollPane.getChildren().add(deskColumns[i]); //add the current deskColumn to the main HBox
                }
                this.scrollPane.setContent(boxToFillScrollPane); //fill the scrollpane with the main HBox

                int noOfClients = simulationInterval / ((minArriveTimeBetweenCustomers + maxArriveTimeBetweenCustomers) / 2 +1); //compute the number of clients

                //create a new ClientGenerator
                ClientGenerator clientGenerator = new ClientGenerator(noOfClients,minArriveTimeBetweenCustomers,maxArriveTimeBetweenCustomers,minServiceTimePerCustomer,maxServiceTimePerCustomer);

                //Create a new Queue Simulator
                QueueSimulator simulator = new QueueSimulator(this, clientGenerator, noOfQueues ,simulationInterval);

                //Start the simulator
                new Thread(simulator).start();
            }
        }else{
            alert("Fields cannot be empty!");
        }
    }


    public void setCounter(String text){

        //update the counter label with the new value (text)
        Platform.runLater(new Runnable(){

            @Override
            public void run() {
                counterLabel.setText(text);
            }
        });
    }


    public VBox createDeskBox(int nr){
        //create a deskBox and return it(VBox with the desk img and number)
        VBox deskBox = new VBox();
        File deskF = new File("assets/desk.png");
        Image desk = new Image(deskF.toURI().toString(),99,99,true,true);
        ImageView deskImg = new ImageView(desk);
        deskBox.getChildren().addAll(new Label(String.format("Desk: %d",nr)),deskImg);

        return deskBox;
    }

    public VBox createStickMan(int clientId, int arrivalTime, int serviceTime, int hasToWait){
        //create a stickMan
        VBox stickManBox = new VBox();

        File manF = new File("assets/man.png");
        Image man = new Image(manF.toURI().toString(),99,99,true,true);
        ImageView manImg = new ImageView(man);

        stickManBox.getChildren().addAll(manImg, new Label(String.format("Client: %d \nArrival: %d\nService time: %d\nhasToWait: %d ", clientId, arrivalTime, serviceTime,hasToWait)));

        return stickManBox;
    }

    public void addStickManToColumn(int columnId, Client client){
        //add stick man to a specific column
        VBox stickMan = createStickMan(client.getId(),client.getArrivalTime(),client.getServiceTime(),client.getHasToWaitTime());

        deskClientMap.get(deskColumns[columnId-1]).add(stickMan);
        Platform.runLater(new Runnable(){

            @Override
            public void run() {
                deskColumns[columnId-1].getChildren().add(stickMan);
            }
        });
    }

    public void removeStickManFromColumn(int columnId){
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                deskColumns[columnId-1].getChildren().remove(deskClientMap.get(deskColumns[columnId-1]).get(0));
                deskClientMap.get(deskColumns[columnId-1]).remove(0);
            }
        });
    }


    public void updateResults(double avgWaitTime, double avgServTime, double emptyQueueTime){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                avgWaitTimeLabel.setText(String.format("%.2f",avgWaitTime));
                avgServiceTimeLabel.setText(String.format("%.2f",avgServTime));
                emptyQueueTimeLabel.setText(String.format("%.2f",emptyQueueTime));
            }
        });
    }


    @FXML public void exitPressed(ActionEvent event){
        System.exit(0);
    }

    @FXML public void clearAllPressed(ActionEvent event){
        minArriveTimeBetweenCustomersField.setText("");
        maxArriveTimeBetweenCustomersField.setText("");
        minServiceTimePerCustomerField.setText("");
        maxServiceTimePerCustomerField.setText("");
        noOfQueuesField.setText("");
        simulationIntervalField.setText("");
        counterLabel.setText("0");
        avgServiceTimeLabel.setText("0");
        avgWaitTimeLabel.setText("0");
        emptyQueueTimeLabel.setText("0");
        scrollPane.setContent(null);
    }
}
