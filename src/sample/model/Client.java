package sample.model;

/**
 * Created by andradafarcas on 28/3/17...
 */

public class Client {
    private int id, arrivalTime, serviceTime;

    //tracks the amount of time a client has to spend at the queue he was added at (service time + time to wait for the customers in fron to be served)
    private int hasToWaitTime = 0;

    public Client(int id, int arrivalTime, int serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public int getId() {
        return id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setHasToWaitTime(int hasToWaitTime){
        this.hasToWaitTime = hasToWaitTime;
    }

    public int getHasToWaitTime(){
        return this.hasToWaitTime;
    }


    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", arrivalTime=" + arrivalTime +
                ", serviceTime=" + serviceTime +
                '}' +
                "\n";
    }
}
