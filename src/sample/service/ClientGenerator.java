package sample.service;

import sample.model.Client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by andradafarcas on 29/3/17...
 */

public class ClientGenerator {

    //logger
    private Logger logger = Logger.getLogger(this.toString());
    private FileHandler fileHandler ;


    int numberOfClients,minArriveTimeBetweenCustomers, maxArriveTimeBetweenCustomers,  minServiceTimePerCustomer, maxServiceTimePerCustomer;


    public ClientGenerator(int noOfClients, int minArriveTimeBetweenCustomers, int maxArriveTimeBetweenCustomers, int minServiceTimePerCustomer, int maxServiceTimePerCustomer){

        this.numberOfClients = noOfClients;
        this.minArriveTimeBetweenCustomers = minArriveTimeBetweenCustomers;
        this.maxArriveTimeBetweenCustomers = maxArriveTimeBetweenCustomers;
        this.minServiceTimePerCustomer = minServiceTimePerCustomer;
        this.maxServiceTimePerCustomer = maxServiceTimePerCustomer;

        try{
            fileHandler = new FileHandler("ServiceLog.log");
        }catch (IOException ex){
            System.err.println("Could not load logger for ClientGenerator. ");
        }

        logger.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);
    }

    public int getRandomValueBetween(int min, int max){
        //Generate a random int from the interval [min,max]
        Random r = new Random();
        return r.nextInt(max-min) + min;
    }


    public List<Client> createClients() {


        int crtArrivalTime = 0;

        //generate a number of clients
        List<Client> clients = new ArrayList<>();
        for (int i = 0; i < numberOfClients; i++) {
            crtArrivalTime +=  getRandomValueBetween(minArriveTimeBetweenCustomers,maxArriveTimeBetweenCustomers);
            int serviceTime = getRandomValueBetween(minServiceTimePerCustomer,maxServiceTimePerCustomer);
            clients.add(new Client(i, crtArrivalTime, serviceTime));
            logger.info("Created client: " + i + " with arrivalTime: " + crtArrivalTime + " and serviceTime: " + serviceTime);
        }

        return clients;
    }

    @Override
    public String toString() {
        return "ClientGenerator{}";
    }
}