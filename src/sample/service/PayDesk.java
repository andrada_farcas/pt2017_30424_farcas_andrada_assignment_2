package sample.service;

import sample.Controller;
import sample.model.Client;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by andradafarcas on 29/3/17...
 */

public class PayDesk extends Thread {
    //controller, in order to update the UI
    Controller controller;

    //keeps track of the current client being served
    Client clientCurent = null;

    //number of seconds to sleep = number of seconds remaining until client is served
    private int toSleep;

    private boolean isBusy;

    //paydesk id
    private int payDeskId;

    //keeps track of the number of seconds the payDesk is empty
    private double isEmptyTime = 0;


    //Queue for keeping the clients in queue at the current paydesk
    private LinkedBlockingQueue<Client> clients = new LinkedBlockingQueue<>();


    //logger
    private Logger logger = Logger.getLogger(this.toString());
    private FileHandler fileHandler;


    public PayDesk(Integer id,Controller ctr) throws IOException {
        this.controller = ctr;
        payDeskId = id;

        //Each paydesk has a separate log file
        fileHandler = new FileHandler("PayDesk " + id.toString() + ".log");
        logger.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);
    }


    //return the number of seconds the queue was empty
    public double getEmptyTime(){
        return this.isEmptyTime;
    }

    //return the paydesk id
    public int getPayDeskId(){
        return payDeskId;
    }

    //return true if paydesk is currently serving a client
    public boolean isBusy(){
        return this.isBusy;
    }


    public void addClient(Client client){
        //add a client to the queue
        this.clients.add(client);
        client.setHasToWaitTime(client.getHasToWaitTime() + toSleep);
        for(Client cl:clients){
            client.setHasToWaitTime(client.getHasToWaitTime() + cl.getServiceTime());
        }
        controller.addStickManToColumn(payDeskId, client);
        logger.info("Client with ID: " + client.getId() + " has been added to the Queue and he has to wait: " + client.getHasToWaitTime() + ".");
    }

    public int clientsSize(){
        //return the number of clients currently in the queue(waiting)
        return clients.size();
    }


    public void run() {
        try {
            while (true) {
                while (toSleep > 0) {
                    //if it is serving a client
                    Thread.sleep(1000);
                    toSleep--;
                }
                if (toSleep == 0) {
                    //if is not serving clients
                    if (clientCurent != null) {
                        // if i have been serving a client
                        logger.info("The client with the ID: " + clientCurent.getId() + " has been served and is leaving the queue.");
                        controller.removeStickManFromColumn(payDeskId);
                        clientCurent = null;
                    }
                    if (clients.size() > 0) {
                        clientCurent = clients.take();
                        logger.info("PayDesk " + payDeskId + " I am serving the client " + clientCurent + ".");
                        toSleep = clientCurent.getServiceTime();
                        isBusy = true;
                    } else {
                        isBusy = false;
                        isEmptyTime+=0.5;
                        Thread.sleep(500);
                    }
                }
            }
        }catch (InterruptedException e) {
            logger.info("!! PayDesk " + payDeskId + " I have been forcefully interrupted !!");
        }
    }

}
