package sample.service;

import sample.Controller;
import sample.model.Client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by andradafarcas on 30/3/17...
 */

public class QueueSimulator extends Thread {
    private Controller controller;
    private ClientGenerator clientGenerator;

    private List<Client> clients = new ArrayList<>();

    private int simulationTime;

    private Logger logger = Logger.getLogger(this.toString());
    private FileHandler fileHandler;

    List<PayDesk> payDesks;

    public QueueSimulator(Controller ctr, ClientGenerator generator, int noOfDesks, int simulationTime) {
        try {
            fileHandler = new FileHandler("Generator.log");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        logger.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);


        this.simulationTime = simulationTime;
        this.clientGenerator = generator;
        this.controller = ctr;


        payDesks = new ArrayList<>();
        for (int i = 1; i <= noOfDesks; i++)
            try {
                payDesks.add(new PayDesk(i, ctr));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        clients = clientGenerator.createClients();
    }

    public void run() {
        try {
            //run simulator

            //keeps track of clients who actually stayed in queue
            List<Client> visited = new ArrayList<>();

            //counter
            int counter = 0;

            //creates a thread pool
            ExecutorService executor = java.util.concurrent.Executors.newFixedThreadPool(payDesks.size());
            for (PayDesk pd : payDesks) {
                executor.execute(pd);
            }

            logger.info("Simulation proccess starts... ");


            //while simulation time is not over
            while (counter <= simulationTime) {

                for (Iterator<Client> iterator = clients.iterator(); iterator.hasNext(); ) {
                    //iterate through the client list

                    //take the current client
                    Client client = iterator.next();

                    //check to see if the client has the arrival time equal to the counter value
                    if (client.getArrivalTime() == counter) {

                        //search for the optimal paydesk to assign the client to
                        PayDesk optimal = payDesks.get(0);

                        //check for optimal paydesk
                        for (PayDesk payDesk : payDesks) {
                            //if paydesk is not busy, or has the lowest number of clients, it becomes the optimal one
                            if (!payDesk.isBusy() || payDesk.clientsSize() < optimal.clientsSize())
                                optimal = payDesk;
                        }

                        //assign the client to the optimal paydesk
                        optimal.addClient(client);
                        logger.info("At time " + counter + " I have assigned the client " + client + " to the payDesk with Id: " + optimal.getPayDeskId());

                        //add the client to the list of clients who visited the shop
                        visited.add(client);

                        //remove the client from the pending list
                        iterator.remove();
                    }
                }

                //sleep for a second
                Thread.sleep(1000);

                //update the counter in the UI
                controller.setCounter(String.valueOf(counter + 1));

                counter++;
            }

            //simulation time over

            //force the paydesks to close their activity (shutdown threads)
            executor.shutdownNow();

            //update the UI (inform the user about the end of simulation time)
            controller.info("Simulation over! Check results --->>>");

            logger.info("");
            logger.info("Simulation over!");

            //calculate average of Clients WaitTime (taking into account only those who were served)
            //calculate average of Clients Service Time (taking into account only those who were served)
            int waitTimeSum = 0;
            int serviceTimeSum = 0;
            for (Client cl : visited) {
                waitTimeSum += cl.getHasToWaitTime();
                serviceTimeSum += cl.getServiceTime();
            }
            double avgWaitTime = (double) waitTimeSum / visited.size();
            double avgServiceTime = (double) serviceTimeSum / visited.size();

            logger.info("Avg wait time: " + avgWaitTime);
            logger.info("Avg service time: " + avgServiceTime);

            //calculate the average Queue empty time from all queues
            int sumEmptyTime = 0;
            for (PayDesk c : payDesks) {
                sumEmptyTime += c.getEmptyTime();
            }
            double avgEmptyTime = (double) sumEmptyTime / payDesks.size();
            //log the avg empty queue time
            logger.info("Avg Queue empty time: " + avgEmptyTime);

            controller.updateResults(avgWaitTime, avgServiceTime, avgEmptyTime);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
